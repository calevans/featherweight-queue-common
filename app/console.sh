#!/usr/bin/env php
<?php
declare(strict_types=1);

use Symfony\Component\Console\Application;


/**
 * @todo add some kind of auto discovery so that other FWQ packages can contain
 *       commands and they automatically be loaded here. e.g. fwq-runner has
 *       QueueRunnerCommand.
 */
$container = require_once realpath(dirname(__FILE__)) . '/bootstrap.php';
$app = new Application('FeatherweightQueue-QueueRunner', '1.0.0');
$app->container = $container;

foreach($container['commands'] as $class) {
  $app->add(new $class());
}

try {
  $app->run();
} catch (\Throwable $e) {
  error_log($e->getMessage()??'ERROR HAS NO MESSAGE');
}

