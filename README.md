# Featherweight Queue<br />
Cal Evans <cal@calevans.com>

# Purpose

FeatherweightQueue exists because there is no light weight queueing system built in PHP. All the good ones are part of frameworks. You shouldn't need a framework to do queueing if you don't want one. The second goal was easy to use. While there are a few things you have to do to get FWQ setup, once you do, it should be reletivly maintenance free. You can run the entire system on a single server if you like. For those that need to scale, you can break it out into multiple servers.



FWQ is designed around the concept that all queued jobs are "Logical Units of Work" (LUW) An LUW has everything it needs to execute. In most cases - e.g. queueing up emails to be sent - this means that the queueRunners do not necessarily need to have access to your system. They can run independently if you give them all the infromation they need in the `parameters` array. Doing it this way will help make them fast. There are cases where you will want your LUW to operate on your systyem. In these cases, you need to make sure that you code you job so that can do so. This means including all of the packages that are needed to complete the job.

FWQ is liteweight. I've included the bare minimum of packages to get the job done so that there is as little code as possible to execute. You the Job developer are the final arbiter of lite. If your jobs load in an entire framework to execute then your jobs will run slower. Stick to the absolute minimum. Less is more.

# Install

1. `composer require eicc/featherweightqueue-common`
1. Create yout `logs` directory
1. Run `console.sh makeQueue --name=low_priority --direction=FIFO` to create your first queue.
1. Run `sample/queueJob.sh` to make sure queueing works.

## OPTIONAL

1. `composer require eicc/featherweightqueue-runner`
2. Run `app/console.sh queue:runner --queue=low_priority --name=GIVE_THE_WORKER_A_UNIQUE_NAME --count=1000` to drain the jobs just queued.


## Currently available Transports

- [File System](https://gitlab.com/calevans/featherweighttransport-filesystem) (For Development Only)

## Planned Transports

- Database
- Reddis
- Beanstalkd
- Amazon SQS
- Gearman


# Design and architectual notes

There are several good queue management systems available for PHP but all at some point miss critical pieces. A good queue should:

- Be liteweight
- Be fast
- Be scalable

This means that there should be an absolute minimum number of dependencies for the system. Implementations of the system may add dependencies based on needs but the queueing system itself should only contain what is necessary to process the queue.

## Parts of the system

In any good queueing system, there are 5 basic parts.

### The Logical Unit of Work

This is code that is to be executed. It is only necessary to be available in the queueRunner environment. LUWs need to be packages as seperate composer packages and required by the queueRunner. Each package can contain one or more classes that are self-contained jobs that can be executed. Each package of LUWs need to require any needed libraries it needs to do the job.

LUWs have no concept of the transport nor do they know they are a LUW. They are entirely self-contained.

LUWs are responsible for notifying other systems of their status. This includes failure.

### The Job

A `Job` is a wrapper around the logical unit of work (LUW) to be completed. The queue runner instantiates a job and then hands it a payload from the transport.

Proposed payload:
```
   {
     jobId: integer|string,
     clientJobId: integer|string,
     dateOriginallySubmited: dateTime,
     maxRetries: integer,
     retries: integer,
     nextRetry: dateTime,
     className:  string,
     queueName: string,
     parameters: []
   }
```

`jobId` is the system assigned ID.

`clientJobId` is assigned by the client.

The Job will instantiate the LUW. Hand it any parameters stored with the object, and then execute it. The Job will then decide next actions based on a return of true or an exception.

1. The job completed sucessfully, mark job as complete and move on
1. The job did not complete successfully, following the requeue logic. If the maximum number of requeues has been hit, mark the job as failed and move on.

A job must be able to do 2 things.

1. Instantiate and execute the object that contains the LUW
1. Requeue itself.

The Job interface and class are only available in the queuerunner environment. The client will simply craft a properly formated json object and then push it into the transport.

The job is reponsible for logging success or failure of jobs but not notifying anyone of failures.

### The Queue

Queues are the interface between the system and the transports.

### The Queue Runner

This is the process that talkes to the queue, pops the next job off the queue, and exeutes it. There may be hundreds of queue runners operating at any given time spread over multiple machines.

### The Transport

This is where the jobs are stored until they are ready for working. Beanstalk, a database, redis, or the file system can all be used as transports. The transport does not care about the contents of the LUW. it deals only with the json_encded version. Everything it needs must be passed to `push()`.

# Random Notes and Thoughts

**Things in this section disapear as they are either implemented or discarded.**

- To keep this library as small as possible, it will not know about any transports. You have to composer require at least 1 transport for this to work.
  - Possible transports:
      * File System (mainly for development)
      * Database
      - Beanstalkd
      - Amazon SQS
      - Gearman
      - Rabbitmq (?)
      - Reddis (?)

- The base install now becomes Common+Transport. If you install common+transport in any existing project, you can queue jobs. If you install QueueRunner you can also run jobs. Or you can install Common+QueueRunner+Transport on a different computer and run the jobs from there. Multiple QUeueRunners can run on the same comptuer as long as you give them different names. Server is only necessary if you want to have a REST interface to queue jobs.
- Move Make/Destroy/Update/Query commands to common.

# TODO

- Write beanstalkd transport
- Create a BaseCommand class that has standard headers and footers pre-defined.
- Update the look and feel of all command line tool output.
- Rename console.sh to fwq.sh
- Create a composer project that sets up a fwq project with the basic composer json that is in sample with the scripts.
