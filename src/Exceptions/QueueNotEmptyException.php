<?php

declare(strict_types=1);

namespace Eicc\Fwq\Exceptions;

class QueueNotEmptyException extends \Exception
{
}
