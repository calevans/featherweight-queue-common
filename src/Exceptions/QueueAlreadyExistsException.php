<?php

declare(strict_types=1);

namespace Eicc\Fwq\Exceptions;

class QueueAlreadyExistsException extends \Exception
{
}
