<?php

declare(strict_types=1);

namespace Eicc\Fwq\Models;

use Eicc\Fwq\Exceptions\MaxRetriesException;
use Eicc\Fwq\Exceptions\InvalidLUWException;
use Eicc\Fwq\Exceptions\FailedJobException;
use Eicc\Fwq\Interfaces\JobInterface;
use Eicc\Fwq\Models\AbstractJob;

/**
 * This is the main code that the queue runner calls. You instantiate it and
 * hand it a LUW. From there it instantiates the class named in the LUW and
 * invokes it, passing in the parameters array.
 *
 * @todo move all dates to UTC
 * @todo finish sanityCheck()
 */
class Job extends AbstractJob
{
  public const SECONDS = 60;

  public function getJobId(): int|string
  {
    return $this->luw->jobId;
  }

  public function execute(): void
  {
    $this->container['log']->debug("BEGIN execute id for JobId :" . $this->luw->jobId);

    try {
      if (!$this->sanityCheck($this->luw)) {
        $this->container['log']->info($this->luw->jobId . " failed it's sanity check. Refusing to execute.");
        throw new InvalidLUWException($this->luw->jobId . " failed it's sanity check. Refusing to execute.");
      }
      $luwJob = $this->objectToExecute;
      $luwJob((array)$this->luw->parameters);

      $this->container['log']->info($this->luw->jobId . " : Job executed.");
    } catch (FailedJobException $e) {
      $this->fail();
    } catch (\Error $e) {
      $this->container['log']->info(
          $this->luw->jobId . " : There was an ERROR with the message " . $e->getMessage()
      );
      // DO NOT REQUEUE
    } catch (\Throwable $e) {
      $this->container['log']->info(
          $this->luw->id . " : There was an unhandled exception with the message " . $e->getMessage()
      );
      // DO NOT REQUEUE
    }
  }

  /**
   * This is for both queueing new jobs and for requeueing a failed job.
   *
   * @todo Return the job as a string
   */
  public function queue(string $queueNameOverride = ''): void
  {

    if (!$this->sanityCheck($this->luw)) {
      throw new InvalidLUWException(" Invalid LUW detected. Not queueing.");
    }

    $this->luw->nextRetry = (
        (new \DateTimeImmutable())->add(
            new \DateInterval('PT' . ( $this->luw->retries * self::SECONDS ) . 'S')
        )
    )->format($_ENV['DATE_FORMAT']);

    $queueName = !empty($queueNameOverride) ? $queueNameOverride : $this->luw->queueName;
    $queue = $this->container['queue']($this->container, $queueName);

    try {
      $queue->push($this->luw, new \DateTimeImmutable($this->luw->nextRetry));
    } catch (InvalidQueueException $e) {
      $this->container['log']->error($e->getMessage());
      throw new InvalidQueueException($e->getMessage());
    }
  }


  /**
   * Check to make sure that all the necessary parameters for the job to execute
   * are here. This doesn't check to make sure the parameters of the luw are
   * correct, jsut the Job parameters.
   * proposed luw payload:
   * {
   *   id: integer|string,
   *   dateOriginallySubmited: dateTime,
   *   maxRetries: integer,
   *   retries: integer,
   *   nextRetry: dateTime,
   *   className:  string,
   *   queueName: string,
   *   runAfter: dateTime,
   *   parameters: []
   * }
     * @todo  flesh this out
   */
  public function sanityCheck(object $luw): bool
  {
    // test for className is a callable? Or just let it fail, log it, and see who notices.
    return true;
  }

  public function success(): void
  {
  }
}
