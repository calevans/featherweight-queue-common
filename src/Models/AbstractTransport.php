<?php

declare(strict_types=1);

namespace Eicc\Fwq\Models;

use Eicc\Fwq\Interfaces\TransportInterface;
use Pimple\Container;

/**
 */
abstract class AbstractTransport implements TransportInterface
{
  /**
   * Meta attributes of a queue and their default value
   */
  protected $meta = [
    'direction' => 'FIFO'
  ];
}
