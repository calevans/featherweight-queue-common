<?php

declare(strict_types=1);

namespace Eicc\Fwq\Models;

use Eicc\Fwq\Interfaces\JobInterface;
use Pimple\Container;

/**
 */
abstract class AbstractJob implements JobInterface
{
  protected ?object $luw;
  protected Container $container;

  public function __construct(object $luw, Container $container)
  {
    $this->luw = $luw;
    $this->container =  $container;
    $className = $this->luw->className;
    $this->objectToExecute = new $className();
  }

  public function fail(): void
  {
    $this->container['log']->info(
        $this->luw->id . " : Job failed with the message " . $e->getMessage() . ". Requeueing"
    );

    if ($this->luw->retries >= $this->luw->maxRetries) {
      $this->queue($_ENV['FAILED_JOB_QUEUE']);
      throw new MaxRetriesException(
          "Job " . $this->luw->id . " has been run it's maximum number of times. Marking it as failed. "
      );
    }

    $this->queue();
  }
}
