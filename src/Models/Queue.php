<?php

declare(strict_types=1);

namespace Eicc\Fwq\Models;

use Pimple\Container;
use Eicc\Fwq\Interfaces\QueueInterface;
use Eicc\Fwq\Exceptions\OutOfBoundsException;
use Eicc\Fwq\Exceptions\InvalidQueueException;

/**
 * A Queue is the interface between the system and the transport. Nothing talks
 * directly to a transport except for a queue.
 */
class Queue implements QueueInterface
{
  protected ?Container $container = null;
  protected ?string $name = '';

  public function __construct(Container $container, string $name)
  {
    $this->container = $container;
    $this->name = $name;
  }

  public function push(object $luw, \DateTimeImmutable $runAfter): void
  {
    $transport = $this->container['transport'];
    $transport->push($luw, $runAfter, $this->name);
  }

  public function pop(string $workerName): object|null
  {
    return $this->container['transport']->pop($this->name, $workerName);
  }

  public function count(): int
  {
    return $this->container['transport']->jobCount($this->name);
  }

  public function list(int $pageNumber = 0, int $perPage = 100): object
  {
    if ($pageNumber < 0) {
      $this->container['log']->error('Page number cannot be less than 0.');
      throw new OutOfBoundsException('Page number cannot be less than 0.');
    }

    $listOfJobs = $this->container['transport']->jobList($this->name);

    if (($pageNumber * $perPage) > count($listOfJobs)) {
      $this->container['log']->error('Requested a page that it past the end of the list of jobs.');
      throw new OutOfBoundsException('Requested a page that it past the end of the list of jobs.');
    }

    if ($pageNumber === 0) {
      return $listOfJobs;
    }

    return array_slice($listOfJobs, $beginning, $perPage);
  }

  public function createQueue(array $parameters): void
  {
    try {
      $this->container['transport']->createQueue($this->name, $parameters);
    } catch (\Exception $e) {
      $this->container['log']->error($e->getMessage());
      throw new \Exception($e->getMessage());
    }
  }

  public function destroyQueue(): void
  {
    try {
      $this->container['transport']->destroyQueue($this->name);
    } catch (InvalidQueueException $e) {
      $this->container['log']->error($e->getMessage());
      throw new InvalidQueueException($e->getMessage());
    }
  }

  public function setSetting(string $key, $value): void
  {
    $this->container['transport']->setSetting($this->name, $key, $value);
  }

  public function getSetting(string $key)
  {
    return $this->container['transport']->getSetting($this->name, $key);
  }

  public function getMeta(string $queueName): object
  {
    return $this->container['transport']->getmeta($this->name);
  }
}
