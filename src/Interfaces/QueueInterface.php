<?php

declare(strict_types=1);

namespace Eicc\Fwq\Interfaces;

use Pimple\Container;

interface QueueInterface
{
  public function __construct(Container $container, string $name);
  public function push(object $luw, \DateTimeImmutable $runAfter): void;
  public function pop(string $workerName): object|null;
  public function count(): int;
  public function list(int $pageNumber = 0, int $perPage = 100): object;
  public function createQueue(array $parameters): void;
  public function destroyQueue(): void;
  public function getMeta(string $queueName): object;
}
