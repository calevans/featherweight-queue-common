<?php

declare(strict_types=1);

namespace Eicc\Fwq\Interfaces;

interface TransportInterface
{
  public function push(object $luw, \DateTimeImmutable $runAfter, string $queueName): void;
  public function pop(string $queueName, string $workerName): object|null;
  public function jobCount(string $queueName): int;
  public function jobList(string $queueName): array;
  public function getSetting(string $queueName, string $key);
  public function setSetting(string $queueName, string $key, $value): void;
  public function createQueue(string $queueName, array $parameters): void;
  public function destroyQueue(string $queueName): void;
  public function doesQueueExist(string $queueName): bool;
  public function listQueues(): array;
  public function getMeta(string $queueName): object;
  public function initalize(): void;
}
