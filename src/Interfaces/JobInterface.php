<?php

declare(strict_types=1);

namespace Eicc\Fwq\Interfaces;

use Pimple\Container;

/**
 * @todo Make a Job base class and flesh out the basics of fail(). fail()
 *       won't change in most cases. The job will be placed in the failed
 *       queue. No need for each job to have to recreate that.
 */
interface JobInterface
{
  public function __construct(object $luw, Container $container);
  public function execute(): void;
  public function queue(): void;
  public function success(): void;
  public function fail(): void;
}
