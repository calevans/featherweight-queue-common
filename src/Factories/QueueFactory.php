<?php

declare(strict_types=1);

namespace Eicc\Fwq\Factories;

use Eicc\Fwq\Interfaces\QueueInterface;
use Eicc\Fwq\Models\Queue;
use Eicc\Fwq\Exceptions\InvalidQueueException;
use Pimple\Container;

class QueueFactory
{
  public static function get(Container $container, string $queueName): QueueInterface
  {
    $transport = $container['transport'];
    return new Queue($container, $queueName);
  }
}
