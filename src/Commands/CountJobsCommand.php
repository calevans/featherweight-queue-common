<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\Commands;

use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class CountJobsCommand extends Command
{
  protected $debug = false;
  protected ?string $queueName;
  protected ?OutputInterface $output = null;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
         new InputOption('queue', '', InputOption::VALUE_REQUIRED, 'The name of the queue to create.'),
        ];

    $this->setName('queue:count')
        ->setDescription('Counts the number of jobs in the specificed queue.')
        ->setDefinition($definition)
        ->setHelp('Counts the number of jobs in the specificed queue.');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();
    $queueName = $input->getOption('queue');

    if (empty($queueName)) {
      throw new NoQueueSpecifiedException();
    }

    $this->output->writeln(
        sprintf(
            '%d jobs in %s',
            $this->getApplication()->container['queue']($this->getApplication()->container,$queueName)->count(),
            $queueName
        ),
        OutputInterface::VERBOSITY_NORMAL
    );

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
