<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\Commands;

use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class UpdateQueueCommand extends Command
{
  protected $debug = false;
  protected ?OutputInterface $output = null;
  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
         new InputOption('queue', '', InputOption::VALUE_REQUIRED, 'The name of the queue to change.'),
         new InputOption('key', '', InputOption::VALUE_REQUIRED, 'The key to update.'),
         new InputOption('value', '', InputOption::VALUE_REQUIRED, 'The new value'),
        ];

    $this->setName('queue:settings:set')
        ->setDescription('Update a setting on a Queue.')
        ->setDefinition($definition)
        ->setHelp('Update a setting on a Queue.');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();
    $queueName = $input->getOption('queue');
    $key = $input->getOption('key');
    $value = $input->getOption('value');

    if (empty($queueName)) {
      throw new NoQueueSpecifiedException();
    }

    if (empty($key)) {
      throw new \Exception('No key specified to update');
    }

    $queue = $this->getApplication()->container['queue']($this->getApplication()->container,$queueName);

    try {
      $queue->setSetting($key, $value);
    } catch (\Exception $e) {
      $this->getApplication()->container['log']->error($e->getMessage());
      throw new \Exception($e->getMessage());
    }

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
