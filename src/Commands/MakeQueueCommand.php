<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\Commands;

use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class MakeQueueCommand extends Command
{
  protected $debug = false;
  protected ?string $queueName;
  protected ?string $direction;
  protected ?OutputInterface $output = null;
  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
         new InputOption('queue', '', InputOption::VALUE_REQUIRED, 'The name of the queue to create.'),
         new InputOption('direction', '', InputOption::VALUE_REQUIRED, 'FIFO or LIFO. Default is FIFO.', 'FIFO'),
        ];

    $this->setName('queue:make')
        ->setDescription('Creates a new queue to add jobs to.')
        ->setDefinition($definition)
        ->setHelp('Before jobs can be added to a queue, you have to run this command to make the queue.');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();
    $this->queueName = $input->getOption('queue');
    $this->direction = $input->getOption('direction');

    if (empty($this->queueName)) {
      throw new NoQueueSpecifiedException();
    }

    $queue = $this->getApplication()->container['queue']($this->getApplication()->container,$this->queueName);
    $parameters = [
        'direction' => $this->direction,
        ];

    try {
      $queue->createQueue($parameters);
    } catch (\Exception $e) {
      $this->getApplication()->container['log']->error($e->getMessage());
      throw new \Exception($e->getMessage());
    }

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
