<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\Commands;

use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class ListQueuesCommand extends Command
{
  protected $debug = false;
  protected ?string $queueName;
  protected ?OutputInterface $output = null;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
    ];

    $this->setName('queue:list')
        ->setDescription('Returns a list of the queues defined.')
        ->setDefinition($definition)
        ->setHelp('Returns a list of the queues defined.');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();

    foreach ($this->getApplication()->container['transport']->listQueues() as $queue) {
      $this->output->writeln($queue, OutputInterface::VERBOSITY_NORMAL) ;
    }

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
