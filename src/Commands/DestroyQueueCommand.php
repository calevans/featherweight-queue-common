<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Eicc\Fwq\Exceptions\QueueEmptyException;

class DestroyQueueCommand extends Command
{
  protected $debug = false;
  protected ?string $queueName;
  protected ?OutputInterface $output = null;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
         new InputOption('queue', '', InputOption::VALUE_REQUIRED, 'The name of the queue to create.'),
        ];

    $this->setName('queue:destroy')
        ->setDescription('Removes a new queue from the system.')
        ->setDefinition($definition)
        ->setHelp('WARNING: CANNOT BE UNDONE!');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();
    $queueName = $input->getOption('queue');

    if (empty($queueName)) {
      throw new NoQueueSpecifiedException();
    }

    $this->getApplication()->container['queue']($this->getApplication()->container,$queueName)->destroyQueue();

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
