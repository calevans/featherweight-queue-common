<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\Commands;

use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class InitalizeSystemCommand extends Command
{
  protected $debug = false;
  protected ?OutputInterface $output = null;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [];

    $this->setName('initalize')
        ->setDescription('Creates the necessary infrastructure for the system to work.')
        ->setDefinition($definition)
        ->setHelp('WARNING: This will erase everything in the system!');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();

    $this->output->writeln('Featherweight Queue - Initalize System');
    $this->output->writeln(' ', OutputInterface::VERBOSITY_DEBUG);

    $this->getApplication()->container['transport']->initalize();

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
